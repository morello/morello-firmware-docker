FROM git.morello-project.org:5050/morello/morello-sdk/morello-sdk:latest

ENV HOME="/home/morello"

SHELL ["/bin/bash", "-c"]

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    autoconf \
    autopoint \
    bc \
    bison \
    build-essential \
    ca-certificates \
    cpio \
    curl \
    device-tree-compiler \
    dosfstools \
    doxygen \
    fdisk \
    flex \
    gdisk \
    gettext-base \
    git \
    libncurses5 \
    libssl-dev \
    libtinfo5 \
    linux-libc-dev-arm64-cross \
    lsb-release \
    m4 \
    mtools \
    pkg-config \
    python-is-python3 \
    rsync \
    snapd \
    unzip \
    uuid-dev \
    wget

RUN git config --global user.name "Morello-Firmware" \
    && git config --global user.email "Morello.Firmware"\
    && git config --global color.ui true

RUN wget https://storage.googleapis.com/git-repo-downloads/repo -O /usr/local/bin/repo \
    && chmod 777 /usr/local/bin/repo

RUN wget -O - https://github.com/Kitware/CMake/releases/download/v3.25.2/cmake-3.25.2-Linux-$(uname -m).tar.gz | \
    tar -xz -C /usr/local/ --strip-components=1

COPY run.sh /
RUN chmod u+x /run.sh
ENTRYPOINT ["/run.sh"]
