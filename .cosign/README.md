# Cosign and morello-firmware-docker

morello-firmware-docker generated containers are signed using [cosign](https://github.com/sigstore/cosign). To verify the validity of a container before downloading it you can use the commands below:
```
$ cosign verify --key cosign.pub git.morello-project.org:5050/morello/morello-firmware-docker/morello-firmware-docker

Verification for git.morello-project.org:5050/morello/morello-firmware-docker/morello-firmware-docker:latest --
The following checks were performed on each of these signatures:
  - The cosign claims were validated
  - The signatures were verified against the specified public key
  - Any certificates were verified against the Fulcio roots.

[{"critical":{"identity":{"docker-reference":"git.morello-project.org:5050/morello/morello-firmware-docker/morello-firmware-docker"},"image":{"docker-manifest-digest":"sha256:9c4161388221017ba7c5904bc3ce4d126caf135788f32500b8a79e823c5d23dd"},"type":"cosign container image signature"},"optional":null}]
```
The public key [cosign.pub](cosign.pub) can be retrieved from the same directory of this README.md file.
