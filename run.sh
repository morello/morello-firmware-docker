#!/usr/bin/env sh

set -eu

chmod 777 /home/morello/workspace

UID=$(id -u)
GID=$(id -g)

echo "morello:x:$UID:$GID::/home:/bin/bash" > /etc/passwd
echo "morello:x:$GID:" > /etc/group

# Run bash to keep container alive
tail -f /dev/null
