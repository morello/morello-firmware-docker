# Introduction

## Docker image for Morello Firmware

This page contains some simple instructions to get you started on building the firmware for Morello.

**To set it up please follow the instructions below.**

# Setup

## Install docker
```
$ curl -sSL https://get.docker.com | sh
```

**Note:** This guide requires to run docker as a non-root user. After the completion of the command above execute:
```
$ dockerd-rootless-setuptool.sh install
```
and follow the instructions on screen. For further information please refer to the [Docker](https://docs.docker.com/) documentation.

## Install docker-compose

Latest: v2.17.2

Installation command:
```
$ sudo curl -L "https://github.com/docker/compose/releases/download/v2.17.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

Provide correct permissions to docker compose:
```
$ sudo chmod +x /usr/local/bin/docker-compose
```

Test docker-compose:
```
$ docker-compose --version
```
# Usage

Create the following workspace structure:

```
firmware/
  |-> workspace/
  |-> docker-compose.yml
```

Create a `docker-compose.yml` file as shown [in this file](docker-compose.yml).

Then, bring up the container:
```
$ docker-compose up -d
```

**Note:** It is important to update always to the latest version of the morello-firmware-docker. If you are unsure on which version you are running instead of the command above execute:
```
$ docker-compose pull
$ docker-compose up -d
```

To enter into the container, run the command:

```
$ docker exec -it -u morello morello-firmware /bin/bash
```

**Note:** Having entered the `morello-firmware` container, the [firmware-user-guide](https://git.morello-project.org/morello/docs/-/blob/morello/mainline/firmware/user-guide.rst) can be followed for instructions to fetch and build the firmware that can be executed inside the container.

## Cleanup the container

To recover the space used by the `morello-firmware` container execute the following commands:

**STEP 1:** Stop the morello-firmware container.

```
$ docker stop morello-firmware
```

**STEP 2:** Remove all the files belonging to the morello-firmware container.

```
$ docker image rm git.morello-project.org:5050/morello/morello-firmware-docker/morello-firmware-docker:latest -f
$ docker image prune
```

For further information please refer to the [Docker](https://docs.docker.com/) documentation.

# Container verification

morello-firmware-docker generated containers are signed using [cosign](https://github.com/sigstore/cosign). To verify the validity of a container before donwloading it please follow the information contained in the [.cosign](.cosign/README.md) directory.
